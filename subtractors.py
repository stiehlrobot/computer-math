"""
Simulation of 16-bit full subtractor circuit
"""

from re import sub
from gates import AND, NAND, OR, NOR, XOR, XNOR, NOT


def half_subtractor(input_A, input_B):
    xor_out = XOR(input_A, input_B)
    not_and_out = AND(NOT(input_A), input_B)
    return xor_out, not_and_out

def full_subtractor(subtractor_position, input_A, input_B, borrow_input):
    print(f"inputs: {input_A} ({input_A * subtractor_position})| {input_B}({input_B * subtractor_position})| {int(borrow_input)}", end='')
    xor_out, and_out = half_subtractor(input_A, input_B)
    difference, and_out_2 = half_subtractor(xor_out, borrow_input)
    borrow_out = OR(and_out, and_out_2)
    print(f" difference: {int(difference)}, {difference * subtractor_position}")
    return borrow_out

def main():
    borrow_out = full_subtractor(2**0, 1, 0, 0)
    borrow_out = full_subtractor(2**1, 0, 1, borrow_out)
    borrow_out = full_subtractor(2**2, 0, 0, borrow_out)
    borrow_out = full_subtractor(2**3, 0, 0, borrow_out)
    borrow_out = full_subtractor(2**4, 0, 0, borrow_out)
    borrow_out = full_subtractor(2**5, 0, 0, borrow_out)
    borrow_out = full_subtractor(2**6, 0, 0, borrow_out)
    borrow_out = full_subtractor(2**7, 0, 0, borrow_out)
    borrow_out = full_subtractor(2**8, 0, 0, borrow_out)
    borrow_out = full_subtractor(2**9, 0, 0, borrow_out)
    borrow_out = full_subtractor(2**10, 0, 0, borrow_out)
    borrow_out = full_subtractor(2**11, 0, 0, borrow_out)
    borrow_out = full_subtractor(2**12, 0, 0, borrow_out)
    borrow_out = full_subtractor(2**13, 0, 0, borrow_out)
    borrow_out = full_subtractor(2**14, 0, 0, borrow_out)
    borrow_out = full_subtractor(2**15, 0, 0, borrow_out)


if __name__ == '__main__':
    main()