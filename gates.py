"""
All logic gates represented as objects
"""

def AND(input_A, input_B):
    if input_A and input_B:
        return True
    else:
        return False

def NAND(input_A, input_B):
    if input_A and input_B:
        return False
    else:
        return True

def OR(input_A, input_B):
    if input_A or input_B:
        return True
    else:
        return False

def NOR(input_A, input_B):
    if input_A or input_B:
        return False
    else:
        return True

def XOR(input_A, input_B):
    if input_A == input_B:
        return False
    else:
        return True

def XNOR(input_A, input_B):
    if input_A == input_B:
        return True
    else:
        return False

def NOT(input_A):
    return not input_A

def main():

    gates = [AND(), NAND(), OR(), NOR(), XOR(), XNOR()]
    for g in gates:
        
        print(f"{type(g).__name__} | 0 | 0 | : {g(0, 0)}")
        print(f"{type(g).__name__} | 0 | 1 | : {g(0, 1)}")
        print(f"{type(g).__name__} | 1 | 0 | : {g(1, 0)}")
        print(f"{type(g).__name__} | 1 | 1 | : {g(1, 1)}")
        print("-"*30)
    n = NOT()
    print(f"{type(n).__name__} | 0 |: {n(0)}")
    print(f"{type(n).__name__} | 1 |: {n(1)}")

    
if __name__ == '__main__':
    main()