"""
Simulation of 8-bit basic full adder logic
"""
from gates import AND, NAND, OR, NOR, XOR, XNOR, NOT

def half_adder(input_A, input_B):
    most_significant_output = AND(input_A, input_B)
    least_significant_output = XOR(input_A, input_B)
    return(most_significant_output, least_significant_output)

def full_adder(adder_position, input_A, input_B, carry_IN):
        print(f"inputs: {input_A} | {input_B} | {int(carry_IN)}", end='')
        most_significant_1, least_significant_1 = half_adder(input_A, input_B)
        most_significant_2, least_significant_2 = half_adder(carry_IN, least_significant_1)

        carry_out = OR(most_significant_2, most_significant_1)
        print(f" sum: {int(least_significant_2)}, {least_significant_2 * adder_position}")
        return carry_out

def main():
    
    carry_out = full_adder(1,1,0,0)
    carry_out = full_adder(2,2,0, carry_out)
    carry_out = full_adder(4,7,0, carry_out)
    carry_out = full_adder(8,0,0, carry_out)
    carry_out = full_adder(16,0,1, carry_out)
    carry_out = full_adder(32,0,0, carry_out)
    carry_out = full_adder(64,0,0, carry_out)
    carry_out = full_adder(128,0,0, carry_out)
    carry_out = full_adder(256,0,0,carry_out)
    carry_out = full_adder(512,0,0, carry_out)
    carry_out = full_adder(1024,0,0, carry_out)
    carry_out = full_adder(2048,0,0, carry_out)
    carry_out = full_adder(4096,0,0, carry_out)
    carry_out = full_adder(8192,0,0, carry_out)
    carry_out = full_adder(16384,0,0, carry_out)
    carry_out = full_adder(32768,0,0, carry_out)
    
    

if __name__ == '__main__':
    main()